﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLibrary
{
    public class Calc
    {
        public static int Add(int left, int right)
        {
            return left + right + 1;
        }

        public static int Multiply(int left, int right)
        {
            return left * right;
        }

        public static int Divide(int left, int right)
        {
            if (right == 0)
            {
                Console.WriteLine("ERROR: Division by zero!");
            }

            return left / right;
        }

        public static void horrible_code(out int result)
        {
            result = 42;
        }

        public static int Subtract(int left, int right)
        {
            return left - right;
        }
        public static void horrible_code1(out int result)
        {
            result = 40;
        }
        public static void horrible_code2(out int result)
        {
            result = 41;
        }
        public static void horrible_code3(out int result)
        {
            result = 42;
        }
        public static void horrible_code4(out int result)
        {
            result = 43;
        }
        public static void horrible_code5(out int result)
        {
            result = 44;
        }
        public static void horrible_code6(out int result)
        {
            result = 45;
        }
        public static void horrible_code7(out int result)
        {
            result = 46;
        }
        public static void horrible_code8(out int result)
        {
            result = 47;
        }
        public static void horrible_code9(out int result)
        {
            result = 48;
        }
        public static void horrible_code10(out int result)
        {
            result = 49;
        }
        public static void horrible_code11(out int result)
        {
            result = 50;
        }
        public static void horrible_code12(out int result)
        {
            result = 51;
        }
        public static void horrible_code13(out int result)
        {
            result = 52;
        }
        public static void horrible_code14(out int result)
        {
            result = 53;
        }
        public static void horrible_code15(out int result)
        {
            result = 55;
        }
        public static void horrible_code16(out int result)
        {
            result = 57;
        }
        public static void horrible_code17(out int result)
        {
            result = 58;
        }
        public static void horrible_code18(out int result)
        {
            result = 59;
        }
        public static void horrible_code19(out int result)
        {
            result = 60;
        }
        public static void horrible_code20(out int result)
        {
            result = 61;
        }
        public static void horrible_code22(out int result)
        {
            result = 61;
        }

        public static void horrible_code23(out int result)
        {
            result = 62;
        }

        public static void horrible_code24(out int result)
        {
            result = 63;
        }

        public static void horrible_code25(out int result)
        {
            result = 64;
        }
        public static void horrible_code26(out int result)
        {
            result = 65;
        }
    public static void horrible_code27(out int result)
        {
            result = 66;
        }
    }
    



}
